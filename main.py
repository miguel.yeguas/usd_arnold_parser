import os, stat, sys, copy, time

os.environ['ILION_PROJECT'] = 'pko'
os.environ['ILION_ENVIRON'] = 'PRODUCTION'
os.environ['SDA_AR_MODE'] = 'local_safe'
os.environ['LD_LIBRARY_PATH'] = 'C:/ilion/deploy/_3rdParty/pixar_usd/_tags/21.08.SDA.PATCHED.06/lib'

from ilion.lib.usd_utils.shot_usd.utils import SHOT_USD_LAYERS
from ilion.lib.production_tracking import CurrentProject, Shot
from ilion.lib.usd_utils.shot_usd import CfxLayerUsd, CfxSublayerUsd, ShotLayerUsd
from ilion.lib.usd_utils.layer_usd import LayerUsd
from ilion.lib.usd_utils.shot_usd.cfx import HAIR
from pxr import Usd, Sdf, UsdGeom, UsdUtils
from ilion.lib.usd_utils.shot_usd.utils import getShotDepartmentOpinions
from ilion.lib.usd_utils.shot_usd.shot import getShotUsdPath
from ilion.lib.filemgr_wrapper import FileMgrWrapper, CheckedOut
from ilion.lib.filemgrlib.file_mgr_exceptions import FileMgrException
from ilion.lib.filemgrlib import FileMgrRender
from ilion.lib.typelib.type_builder import TypeBuilder
from ilion.tagger import cfx_shot as tagger
import logging
from ilion.lib.typelib.type_builder import TypeBuilder
from datetime import datetime

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
fh = logging.FileHandler("log/app_{}.log".format(datetime.now().strftime("%y%m%d_%H%M")))
LOG.addHandler(fh)
fmw = FileMgrWrapper()
global asset, prims

def print_file(text, *args):
    # print(text)       
    # for t in args:
    #     print("{}\n".format(t))
    # return
    with open('test_python_append.log', 'a') as f:
        f.write("{}\n".format(text))
        [f.write("{}\n".format(t)) for t in args]

def get_sublayers(filename, coiffeur_list, level=0):
    file = os.path.normpath(filename)
    print_file("Level: {}, File: {}".format(level, file))
    # try:
    #     file = fmw.getLatestPath(filename)
    # except FileMgrException as err:
    #     file = filename.replace('/', '\\')
    # else:
    #     file = fmw.getFile(file)
        
    
    # print_file(file)
    # print(file)
    # file = file['real_version']['remote_path']
    # print("remote_path: {}\n".format(file))
    try:
        if Usd.Stage.IsSupportedFile(file):
            stage = Usd.Stage.Open(file)
            if [p for p in stage.TraverseAll() if p.GetTypeName() == "ArnoldCoiffeurGroom" or p.GetTypeName() == "ArnoldCoiffeurPlumage"] != []:
                # print_file("File {}\nLevel {}\n".format(filename, level))
                coiffeur_list.append(filename)
                # print([method_name for method_name in dir(p)])
                # for attr in [attr for attr in p.GetAttributes() if not attr.GetName().startswith("arnold:")]:
                # to_delete = []
                # stage.SetEditTarget(Usd.EditTarget(stage.GetSessionLayer()))
                # for attr in p.GetAttributes():
                #     p.CreateAttribute("arnold:{}".format(attr.GetName()), attr.GetTypeName(), False, attr.GetVariability()).Set(attr.Get())
                #     to_delete.append(attr.GetName())
                # print([p.RemoveProperty(at) for at in to_delete])
                # print(p.GetAttributes())
                # return
                # print(p.GetPath().pathString, p.GetTypeName())
            stage.Unload()
    except Exception:
        pass
    
    try:
        l = Sdf.Layer.FindOrOpen(file)
        for item in l.subLayerPaths:
            print_file("Sublayer: {}".format(item))
            get_sublayers(item, coiffeur_list, level+1)
    except Exception:
        pass

def parse_sequences():
    with open('test_python_append.log', 'w') as f:
        pass
    coiffeur_nodes = []
    sequences = {seq.getStringName(): seq for seq in CurrentProject().getSequences()}
    for sequence_name, seq in sequences.items():
        for shot in seq.getShots():
            hair_layer = CfxLayerUsd(sequence_name, shot.short_name, config=SHOT_USD_LAYERS.get(HAIR), discipline=HAIR, ignore_upload=True)
            for sublayer_obj in hair_layer.sublayers_objs:

                for filename in sublayer_obj.getAllFilenamesRecursive():
                    print_file("SEQ: {}, SHOT: {}".format(sequence_name, shot.short_name))
                    get_sublayers(filename, coiffeur_nodes, 0)
    print_file(set(coiffeur_nodes))
    
def read_coiffeur_nodes():
    files = []
    import json
    with open('test_python_append.log', 'r') as f:
        for l in f.readlines():
            if l.startswith("set"):
                files = l[5:-3].replace("'",'').replace(' ', '').split(',')
    files.sort()
    with open('files.json', 'w') as f:
        json.dump({"files": list(files)}, f)

def parse_coiffeur_nodes():
    import json
    with open('files.json', 'r') as f:
        files = json.load(f)['files']
    sequences = {}
    for file in files:
        fields = file.split('/')
        try:
            seq = fields[3]
            shot = fields[5]
            if seq not in list(sequences.keys()):
                sequences[seq] = set(shot)
            else:
                sequences[seq].add(shot)
        except:
            print(file, fields)
    
    print("Num sequences: {}".format(len(sequences.keys())))
    l = 0
    for k, v in sequences.items():
        l += len(v)
        print("SEQ: {}, SHOTS: {}".format(k, len(v)))
    print("Total shots {}".format(l))

def get_sequence(file):
    return file.split('/')[3]

def get_shot(file):
    return file.split('/')[5]

def modify_files(sequence='', shot=''):
    import json
    with open('files.json', 'r') as f:
        files = json.load(f)['files']
    files_to_check = files if sequence == '' else [file for file in files if get_sequence(file) == sequence]
    for file in files_to_check:
        file_info = fmw.getFile(file)
        print(file_info)
        stage = Usd.Stage.OpenMasked(file, Usd.StagePopulationMask([Sdf.Path('/')]))
        for p in [p for p in stage.TraverseAll() if p.GetTypeName() == "ArnoldCoiffeurGroom" or p.GetTypeName() == "ArnoldCoiffeurPlumage"]:
            to_delete = []
            for attr in p.GetAttributes():
                p.CreateAttribute("arnold:{}".format(attr.GetName()), attr.GetTypeName(), False, attr.GetVariability()).Set(attr.Get())
                to_delete.append(attr.GetName())
            print([p.RemoveProperty(at) for at in to_delete])
        stage.GetRootLayer().Export("new_file.usda")
        break
ARNOLD_PREFIX = "arnold"

fm_effects = FileMgrRender('rep_effects')
REMOTE_EFFECTS_UNIT = fm_effects.getRemoteFullPath('/')

def callback(path):
    global prims
    global asset
    prim = asset.GetPrimAtPath(path)
    if prim and (prim.typeName.startswith("Arnold")):
        for _, prop in prim.properties.items():
            if not prop.name.startswith('{}:'.format(ARNOLD_PREFIX)):
                prims.append(prim)
                break

def loop_layer(layer):
    if len(layer.subLayerPaths) > 0:
        for a_p in layer.subLayerPaths:
            print("Sublayer")
            print(a_p)
            layer = Sdf.Layer.FindOrOpen(a_p)
            layer.Traverse(layer.pseudoRoot.path, callback)
            print(prims)

def get_local_real_version(path):
    latest = fmw.getLatestPath(path)
    real_version = fmw.getRealVersionPath(latest)
    metadata = fmw.findFile(real_version)
    return metadata.get('local').get('path')

global checkedout_files
checkedout_files = []

def checkout(path):
    global checkedout_files
    path_latest = fmw.getLatestPath(path)
    try:
        f = fmw.checkoutSafe(path_latest)[0]
        time.sleep(0.25)
    except Exception:
        LOG.error("Could not checkout {}".format(path_latest))
        return False
    else:
        LOG.debug('Checking out {}'.format(f))
        checkedout_files.append(path_latest)
        return f
def undocheckedout(f=""):
    global checkedout_files
    if f != "":
        if f in checkedout_files:
            fmw.undocheckout(f)
            LOG.debug('Undo Checked out {}'.format(f))
            checkedout_files.remove(f)
    else:
        for f in checkedout_files:
            fmw.undocheckout(f)
            LOG.debug('Undo Checked out {}'.format(f))
        checkedout_files = []

def checkin(file):
    global checkedout_files
    if file in checkedout_files:
        LOG.debug("Checking in {}".format(file))
        try:
            fmw.checkin(file)
            time.sleep(1)
        except Exception:
            LOG.error("Could not checkin {}".format(file))
        checkedout_files.remove(file)
    else:
        LOG.error("File {} not checkedout".format(file))

def compare_filenames(file_a, file_b):
    return fmw.findFile(file_a)['file']['filename'] == fmw.findFile(file_b)['file']['filename']

def modify_hair_layer(sequence_name, shot_name):
    LOG.debug("SEQ {} SHOT {}".format(sequence_name, shot_name))
    hair_layer = CfxLayerUsd(sequence_name, shot_name, discipline=HAIR, ignore_upload=True)
    f = fmw.findFile(hair_layer.filename)
    if not f:
        LOG.error("Could not find file {}\n".format(hair_layer.filename))
        return
    LOG.debug("Reading CfxLayer {}".format(hair_layer.filename))
    hair_l = Sdf.Layer.FindOrOpen(hair_layer.filename)
    if hair_l == None:
        LOG.error("Could not open the layer file {}\n".format(hair_layer.filename))
        return
    sublayers_to_change = []
    for i, sublayer_path in enumerate(hair_l.subLayerPaths):
        LOG.debug("Reading SubLayer {}".format(sublayer_path))
        sublayer = Sdf.Layer.FindOrOpen(sublayer_path)
        if sublayer == None:
            LOG.error("Could not open the layer file {}\n".format(sublayer_path))
            continue
        assets_to_change = []
        for j, asset_path in enumerate(sublayer.subLayerPaths):
            LOG.debug("Reading Asset {}".format(asset_path))

            global asset
            global prims
            asset = Sdf.Layer.FindOrOpen(asset_path)
            prims = []
            asset.Traverse(asset.pseudoRoot.path, callback)
            if prims != []:
                assets_to_change.append((j, asset_path))
        if assets_to_change != []:
            sublayers_to_change.append((i, sublayer_path, copy.copy(assets_to_change)))
    if sublayers_to_change != []:
        checkedout_files = []

        LOG.debug("sublayers to modify")
        LOG.debug(sublayers_to_change)
        
        hair_layer_latest = checkout(hair_layer.filename)
        if hair_layer_latest == False:
            sys.exit()
        LOG.debug("Writing in shdeptop: {}".format(hair_layer_latest))
        try:
            hair_l = Sdf.Layer.FindOrOpen(hair_layer_latest)
            if hair_l.permissionToSave == False:
                LOG.debug("Do not have permission to write in {}\n".format(hair_layer_latest))
                undocheckedout()
                sys.exit()
            for i, sublayer_path in enumerate(hair_l.subLayerPaths):
                for _, sublayer_file, assets_to_change in sublayers_to_change:
                    if compare_filenames(sublayer_path, sublayer_file):
                        sublayer_latest = checkout(sublayer_path)
                        if sublayer_latest == False:
                            undocheckedout()
                            sys.exit()
                        LOG.debug("Writing in cfxsub: {}".format(sublayer_latest))
                        sublayer = Sdf.Layer.FindOrOpen(os.path.normpath(sublayer_latest))
                        if sublayer.permissionToSave == False:
                            LOG.debug("Do not have permission to write in {}".format(sublayer_latest))
                            undocheckedout()
                            sys.exit()
                        for j, asset_path in enumerate(sublayer.subLayerPaths):
                            for _, asset_file in assets_to_change:
                                if compare_filenames(asset_path, asset_file):
                                    asset_latest = checkout(asset_path)
                                    if asset_latest == False:
                                        undocheckedout()
                                        sys.exit()
                                    LOG.debug("Writing in CFX subop opinion: {}".format(asset_latest))
                                    asset = Sdf.Layer.FindOrOpen(os.path.normpath(asset_latest))
                                    if not os.access(os.path.normpath(asset_latest), os.W_OK):
                                        LOG.debug("No permission to write in {}".format(os.path.normpath(asset_latest)))
                                        undocheckedout()
                                        sys.exit()
                                    if asset.permissionToSave == False:
                                        LOG.debug("Do not have permission to write in {}".format(os.path.normpath(asset_latest)))
                                        undocheckedout()
                                        sys.exit()
                                    prims = []
                                    asset.Traverse(asset.pseudoRoot.path, callback)
                                    if prims != []:
                                        for prim in prims:
                                            for _, prop in prim.properties.items():
                                                if not prop.name.startswith(ARNOLD_PREFIX):
                                                    prop.name = "{}:{}".format(ARNOLD_PREFIX, prop.name)
                                        asset.Save()
                                        checkin(asset_latest)
                                    real = get_local_real_version(asset_path)
                                    LOG.debug("cfxsub points to: {}".format(real))
                                    sublayer.subLayerPaths[j] = real
                                    break
                        sublayer.Save()
                        checkin(sublayer_latest)
                        real = get_local_real_version(sublayer_latest)
                        LOG.debug("shdeptop points to: {}".format(real))
                        hair_l.subLayerPaths[i] = real
                        break
            hair_l.Save()
            checkin(hair_layer_latest)
        except Exception as err:
            LOG.error(err)
            undocheckedout()
        else:
            # tag 
            latest_file = fmw.findFile(hair_layer_latest)
            latest_version = latest_file.get('real_version', {}).get('number')
            LOG.debug("Tagging {} as stable v{}".format(hair_layer_latest, latest_version))
            LOG.debug(tagger.tagCfxShotOpinion(sequence_name, shot_name, HAIR, latest_version))

def _read_up_to_cfxsubop(file_path, level=0):
    file_type = TypeBuilder(file_path).getTranslationValue('[fileType]')
    result = {"file_type": file_type, "path":file_path, "subfiles":[] }
    layer = Sdf.Layer.FindOrOpen(file_path)
    if layer == None:
        return None
    if file_type != 'cfxsubop':
        for sublayer_path in layer.subLayerPaths:
            r = _read_up_to_cfxsubop(sublayer_path, level+1)
            if r != None:
                result["subfiles"].append(r)
    else:
        arnold_primitives = []
        def c(path):
            prim = layer.GetPrimAtPath(path)
            if prim and (prim.typeName.startswith("Arnold")):
                for _, prop in prim.properties.items():
                    if not prop.name.startswith('{}:'.format(ARNOLD_PREFIX)):
                        arnold_primitives.append(prim)
                        break
        layer.Traverse(layer.pseudoRoot.path, c)
        if arnold_primitives != []:
            result['arnold_primitives'] = True
        else:
            return None
    return result

def _read_files(file_dict, parent=""):
    LOG.debug("{}:{}".format(file_dict['file_type'], file_dict['path']))
    LOG.debug("Len subfiles {}".format(len(file_dict['subfiles'])))
    modified_files = []
    modified = False
    for file in file_dict['subfiles']:
        modified = _read_files(file, parent=file_dict['path'])
        if modified:
            modified_files.append(file['path'])

    if modified_files != []:
        exported = False
        latest_path = checkout(file_dict['path'])
        if latest_path == False:
            undocheckedout()
            return modified
        layer = Sdf.Layer.FindOrOpen(os.path.normpath(latest_path))
        founded = [False for _ in modified_files]
        LOG.debug("modified_files: {}".format(modified_files))
        if len(layer.subLayerPaths) == 0:
            LOG.debug("Empty layer {}".format(latest_path))
            if parent.find("shdeptop") != -1:
                # Check if parent points to latest
                parent_layer = Sdf.Layer.FindOrOpen(os.path.normpath(parent))
                if parent_layer != None:
                    for sublayer_path in parent_layer.subLayerPaths:
                        if compare_filenames(sublayer_path, latest_path):
                            latest_version = fmw.findFile(latest_path)['real_version']['number']
                            stable_version = fmw.findFile(sublayer_path)['real_version']['number']
                            if latest_version == stable_version:
                                LOG.debug("Stable version points to latest empty version")
                            else:
                                LOG.debug("Stable version does not point to latest empty version")
                                fmw.getFile(file_dict['path'])
                                layer = Sdf.Layer.FindOrOpen(os.path.normpath(file_dict['path']))
                                exported = True
                            break
        for i, sublayer_path in enumerate(layer.subLayerPaths):
            for j, file_path in enumerate(modified_files):
                if compare_filenames(sublayer_path, file_path):
                    layer.subLayerPaths[i] = get_local_real_version(file_path)
                    founded[j] = True
                    break
        LOG.debug("founded: {}".format(founded))
        if all(found == True for found in founded):
            if exported:
                layer.Export(latest_path)
            else:
                layer.Save()
            checkin(latest_path)
            modified = True
        else:
            Sdf.Layer.Reload(layer)
            modified = False
            undocheckedout()

    k = 'arnold_primitives'
    if k in file_dict.keys():
        latest_path = checkout(file_dict['path'])
        if latest_path == False:
            undocheckedout()
            return modified
        layer = Sdf.Layer.FindOrOpen(os.path.normpath(latest_path))
        arnold_primitives = []
        def c(path):
            prim = layer.GetPrimAtPath(path)
            if prim and (prim.typeName.startswith("Arnold")):
                for _, prop in prim.properties.items():
                    if not prop.name.startswith('{}:'.format(ARNOLD_PREFIX)):
                        arnold_primitives.append(prim)
                        break
        layer.Traverse(layer.pseudoRoot.path, c)
        LOG.debug("arnold_primitives {}".format(arnold_primitives))
        if arnold_primitives != []:
            for arnold_primitive in arnold_primitives:
                for _, prop in arnold_primitive.properties.items():
                    if not prop.name.startswith(ARNOLD_PREFIX):
                        prop.name = "{}:{}".format(ARNOLD_PREFIX, prop.name)
            layer.Save()
            checkin(latest_path)
            return True
        else:
            # Latest version contains already arnold, so just update parent
            modified = True
            undocheckedout(latest_path)
    return modified

sequences = {seq.getStringName(): seq for seq in CurrentProject().getSequences()}
for k, v in sequences.items():
    for shot in v.getShots():
        LOG.debug("SEQ {} SHOT {}".format(k, shot.short_name))
        modified = False
        try:
            hair_layer = CfxLayerUsd(k, shot.short_name, discipline=HAIR, ignore_upload=True)
            if hair_layer is None:
                continue
            hair_layer_path = hair_layer.filename
            files_to_read = _read_up_to_cfxsubop(hair_layer_path)
            if files_to_read == None:
                continue
            modified = _read_files(files_to_read)
        except Exception as err:
            LOG.error(err)
            undocheckedout()
        else:
            if modified == True:
                hair_layer_latest = fmw.getLatestPath(hair_layer_path)
                latest_file = fmw.findFile(hair_layer_latest)
                latest_version = latest_file.get('real_version', {}).get('number')
                LOG.debug("Tagging {} as stable v{}".format(hair_layer_latest, latest_version))
                LOG.debug(tagger.tagCfxShotOpinion(k, shot.short_name, HAIR, latest_version))
        for checkedout in checkedout_files:
            LOG.debug("in Checked out: {}".format(checkedout))